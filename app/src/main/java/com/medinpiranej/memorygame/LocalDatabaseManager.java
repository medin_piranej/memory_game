package com.medinpiranej.memorygame;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.medinpiranej.memorygame.Models.PlayerModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Medin Piranej
 */

public class LocalDatabaseManager {

    private static SharedPreferences preferences;

    private static SharedPreferences getPreferences(@NonNull Activity activity){
        if(preferences == null){
            preferences = activity.getSharedPreferences("memory_game",Context.MODE_PRIVATE);
        }
        return preferences;
    }

    public static PlayerModel getLastPlayer(@NonNull Activity activity){
        String last_player = getPreferences(activity).getString("last_player","kitten");
        if(last_player.equals("kitten")){
            return null;
        }
        else{
            return new Gson().fromJson(last_player,PlayerModel.class);
        }
    }

    public static String getLastTag(@NonNull Activity activity){
        return getPreferences(activity).getString("last_tag","");
    }

    public static void setLastTag(@NonNull Activity activity,@NonNull String tag){
        getPreferences(activity).edit().putString("last_tag",tag).apply();
    }

    public static String getLastPicture(@NonNull Activity activity){
        return getPreferences(activity).getString("last_picture","");
    }

    public static void setLastPicture(@NonNull Activity activity,@NonNull String picture){
        getPreferences(activity).edit().putString("last_picture",picture).apply();
    }

    public static void setLastPlayer(@NonNull Activity activity, PlayerModel player){
        if(player == null){
             getPreferences(activity).edit().putString("last_player","").apply();
        }
        else{
            long previousPlayerBestTime = getPlayerBestTimeByName(activity,player.getName());
            if(player.getBestScore() < previousPlayerBestTime){
                // adding to high score players
                List<PlayerModel> playerList = null;
                PlayerModel [] playersArray = getHighScorePlayers(activity);
                if(playersArray != null){
                    //playerList = Arrays.asList(playersArray);// Cannot use it because it will make the list unmodifiable
                    playerList = new ArrayList<>();
                    playerList.addAll(Arrays.asList(playersArray));// TODO to be tested
//                    for(PlayerModel playerModel : playersArray){
//                        playerList.add(playerModel);
//                    }
                }

                if(previousPlayerBestTime != Long.MAX_VALUE && playersArray != null){
                    // player exists on top list, removing it
                    List<PlayerModel> toBeDeleted = new ArrayList<>();
                    for (PlayerModel currentPlayer: playerList) {
                        if(currentPlayer.getName().equals(player.getName())){
                            toBeDeleted.add(currentPlayer);
                        }
                    }
                    // TO avoid java.util.ConcurrentModificationException from list element removal
                    for (PlayerModel currentPlayer: toBeDeleted) {
                        if(currentPlayer.getName().equals(player.getName())){
                            playerList.remove(currentPlayer);
                        }
                    }

                }
                if(playerList == null){
                    // first Player
                    playersArray = new PlayerModel[1];
                    playersArray[0] = player;
                    setHighScorePlayers(activity,playersArray);
                }
                else{
                    //adding it on the list in the current order
                    boolean inserted = false;
                    for(int i = 0; i < playerList.size(); i++){
                        if(player.getBestScore() < playerList.get(i).getBestScore()){
                            playerList.add(i,player);
                            inserted = true;
                            break;
                        }
                    }
                    if(!inserted){
                        playerList.add(player);
                    }
                    if(playerList.size() > 5){// removing last best score
                        playerList.remove(playerList.size() - 1 );
                    }
                    setHighScorePlayers(activity,playerList.toArray(new PlayerModel[playerList.size()]));
                }

            }
            getPreferences(activity).edit().putString("last_player",new Gson().toJson(player)).apply();
        }
    }

    public static long getPlayerBestTimeByName(@NonNull Activity activity,String playerName){
        PlayerModel [] bestPlayers = getHighScorePlayers(activity);
        if(bestPlayers == null || bestPlayers.length == 0){
            return Long.MAX_VALUE;
        }
        else{
            for(PlayerModel player : bestPlayers){
                if(player.getName().equals(playerName)){
                    return player.getBestScore();
                }
            }
            return Long.MAX_VALUE;
        }
    }

    public static PlayerModel [] getHighScorePlayers(@NonNull Activity activity){
        String high_score = getPreferences(activity).getString("high_score","");
        if(high_score.equals("")){
            return null;
        }
        else{
            return new Gson().fromJson(high_score,PlayerModel[].class);
        }
    }

    public static void setHighScorePlayers(@NonNull Activity activity, PlayerModel [] players){
        if(players == null){
            getPreferences(activity).edit().putString("high_score","").apply();
        }
        else{
            getPreferences(activity).edit().putString("high_score",new Gson().toJson(players)).apply();
        }
    }
}
