package com.medinpiranej.memorygame;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.medinpiranej.memorygame.Models.FlickrResponseModel;
import com.medinpiranej.memorygame.Models.PlayerModel;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by Medin Piranej
 */

public class GameSettingsActivity extends Activity {

    public final String TAG = "GameSettingsActivity";

    EditText userName;
    EditText category;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_settings);

        userName = (EditText) findViewById(R.id.userName);
        category = (EditText) findViewById(R.id.category);

        PlayerModel lastPlayer = LocalDatabaseManager.getLastPlayer(this);
        if(lastPlayer != null){
            userName.setText(lastPlayer.getName());
        }
        category.setText(LocalDatabaseManager.getLastTag(this));

        Button startBtn = (Button) findViewById(R.id.startBtn);
        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(userName.getText().length() == 0){
                    Toast.makeText(getApplicationContext(),R.string.please_tell_me_your_name,Toast.LENGTH_SHORT).show();
                }
                else {
                    LocalDatabaseManager.setLastTag(GameSettingsActivity.this,category.getText().length() > 0 ? category.getText().toString() : category.getHint().toString());
                    final ProgressDialog progressDialog = GenericUtils.showProgressDialog(GameSettingsActivity.this, "Loading Photos ...", "Please Wait ...");
                    FlickrApi.getFlickrPhotos(category.getText().length() > 0 ? category.getText().toString() : category.getHint().toString(), 10, new Callback() {
                        @Override
                        public void onFailure(Call call,final IOException e) {
                            progressDialog.dismiss();
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), "Error fetching photos ! exception : " + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }

                        @Override
                        public void onResponse(Call call,final Response response){
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    progressDialog.dismiss();
                                    if (response.isSuccessful()) {
                                        try {
                                            String responseBody = response.body().string();
                                            FlickrResponseModel flickrResponseModel = new Gson().fromJson(responseBody, FlickrResponseModel.class);
                                            if (flickrResponseModel.getPhotos().getPhoto().length >= 10) {
                                                Intent intent = new Intent(getApplicationContext(), GameActivity.class);
                                                intent.putExtra("photos", responseBody);
                                                intent.putExtra("username", userName.getText().toString());
                                                startActivity(intent);
                                                finish();
                                            } else {
                                                Toast.makeText(getApplicationContext(), "Could not find enough photos with given tag. Please write another tag !", Toast.LENGTH_SHORT).show();
                                            }
                                            Log.d(TAG, "Successfully received photos from flickr. response body = " + responseBody);
                                        }
                                        catch (Exception e){
                                            e.printStackTrace();
                                            Toast.makeText(getApplicationContext(), "Could not load photos ! exception : " + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(getApplicationContext(), "Error fetching photos ! error code : " + response.code(), Toast.LENGTH_LONG).show();
                                        Log.e(TAG, "Error fetching photos ! response = " + response.toString());
                                    }
                                }
                            });
                        }
                    });
                }
            }
        });
    }

}
