package com.medinpiranej.memorygame;

import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.medinpiranej.memorygame.Models.FlickrPhotoModel;
import com.medinpiranej.memorygame.Models.FlickrResponseModel;
import com.medinpiranej.memorygame.Models.HighScoreViewHolder;
import com.medinpiranej.memorygame.Models.PlayerModel;
import com.squareup.picasso.Picasso;

import io.fabric.sdk.android.Fabric;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    final String TAG = "MainActivity";
    Button startBtn;
    ListView highScoreListView;
    LinearLayout highScoreContainer;
    ImageView backgroundSliderContainer;
    List<PlayerModel> bestPlayers = new ArrayList<>();

    EditText newBestName;
    EditText newBestTime;
    Button addBtn;
    Button clearBtn;

    List<FlickrPhotoModel> sliderPhotos = new ArrayList<>();
    int sliderCounter = 0;

    void slide(){
        try {
            if (sliderPhotos.size() > 0) {
                if (sliderCounter >= sliderPhotos.size()) {
                    sliderCounter = 0;
                }
                String url = FlickrApi.buildUrl(sliderPhotos.get(sliderCounter), false);
                LocalDatabaseManager.setLastPicture(this,url);
                Picasso.with(this)
                        .load(url)
                        .into(backgroundSliderContainer);
                sliderCounter++;
            }
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    slide();
                }
            }, 5000);
        }
        catch (Exception e){
            e.printStackTrace();
            // stops slider in case of an exception, usually thrown by Glide when activity is being destroyed
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
        PlayerModel [] bestPlayerArray = LocalDatabaseManager.getHighScorePlayers(this);
        if(bestPlayerArray != null && bestPlayerArray.length > 0){
            highScoreContainer.setVisibility(View.VISIBLE);
            bestPlayers = Arrays.asList(bestPlayerArray);
            highScoreListView.setAdapter(new HighScoreAdapter());
        }
        else{
            highScoreContainer.setVisibility(View.GONE);
        }

        FlickrApi.getFlickrPhotos(LocalDatabaseManager.getLastTag(this), 10, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    String responseBody = response.body().string();
                    FlickrResponseModel flickrResponseModel = new Gson().fromJson(responseBody, FlickrResponseModel.class);
                    if (flickrResponseModel.getPhotos().getPhoto().length > 0) {
                        sliderPhotos = Arrays.asList(flickrResponseModel.getPhotos().getPhoto());
                    }
                    Log.d(TAG, "Successfully received photos from flickr. response body = " + responseBody);
                } else {
                    Log.e(TAG, "Error fetching photos ! response = " + response.toString());
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        startBtn = (Button) findViewById(R.id.startBtn);
        highScoreListView = (ListView) findViewById(R.id.highScoreListView);
        highScoreContainer = (LinearLayout) findViewById(R.id.highScoreContainer);
        backgroundSliderContainer = (ImageView) findViewById(R.id.backgroundSliderContainer);

        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),GameSettingsActivity.class));
            }
        });


        newBestName = (EditText) findViewById(R.id.newBestName);
        newBestTime = (EditText) findViewById(R.id.newBestTime);
        addBtn = (Button) findViewById(R.id.addBtn);
        clearBtn = (Button) findViewById(R.id.clearBtn);

        clearBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocalDatabaseManager.setHighScorePlayers(MainActivity.this,null);
                bestPlayers = new ArrayList<>();
                highScoreListView.setAdapter(new HighScoreAdapter());
            }
        });

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlayerModel playerModel = new PlayerModel();
                playerModel.setBestScore(Long.parseLong(newBestTime.getText().toString()));
                playerModel.setName(newBestName.getText().toString());
                LocalDatabaseManager.setLastPlayer(MainActivity.this,playerModel);

                PlayerModel [] bestPlayerArray = LocalDatabaseManager.getHighScorePlayers(MainActivity.this);
                if(bestPlayerArray != null && bestPlayerArray.length > 0){
                    highScoreContainer.setVisibility(View.VISIBLE);
                    bestPlayers = Arrays.asList(bestPlayerArray);
                    highScoreListView.setAdapter(new HighScoreAdapter());
                }
                else{
                    highScoreContainer.setVisibility(View.GONE);
                }
            }
        });

        slide();
        // trying to load last picture on the slider
        String lastPicture = LocalDatabaseManager.getLastPicture(this);
        if(!lastPicture.equals("")){
            Picasso.with(this)
                    .load(lastPicture)
                    .into(backgroundSliderContainer);
        }
    }

    class HighScoreAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return bestPlayers.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View recycledView, ViewGroup parent) {
            View viewToReturn;
            HighScoreViewHolder viewHolder;
            PlayerModel currentPlayer = bestPlayers.get(position);
            if(recycledView == null) {
                viewHolder = new HighScoreViewHolder();
                viewToReturn = GenericUtils.getViewByXmlLayout(getApplicationContext(), R.layout.high_score_listview_item);
                viewHolder.classification = (TextView) viewToReturn.findViewById(R.id.classification);
                viewHolder.name = (TextView) viewToReturn.findViewById(R.id.name);
                viewHolder.time = (TextView) viewToReturn.findViewById(R.id.time);
                viewToReturn.setTag(viewHolder);
            }
            else{
                viewToReturn = recycledView;
                viewHolder = (HighScoreViewHolder) viewToReturn.getTag();
            }

            viewHolder.classification.setText("" + (position + 1));
            viewHolder.name.setText(currentPlayer.getName());
            viewHolder.time.setText("" + currentPlayer.getBestScore());
            return viewToReturn;
        }
    }
}
