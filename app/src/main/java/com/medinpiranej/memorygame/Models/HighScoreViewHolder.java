package com.medinpiranej.memorygame.Models;

import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by Medin Piranej
 */

public class HighScoreViewHolder {
    public TextView classification;
    public TextView name;
    public TextView time;
}
