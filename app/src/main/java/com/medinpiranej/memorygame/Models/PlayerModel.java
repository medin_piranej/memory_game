package com.medinpiranej.memorygame.Models;

/**
 * Created by Medin Piranej
 */

public class PlayerModel {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getBestScore() {
        return bestScore;
    }

    public void setBestScore(long bestScore) {
        this.bestScore = bestScore;
    }

    private String name;
    private long bestScore;
}
