package com.medinpiranej.memorygame.Models;

/**
 * Created by Medin Piranej
 */

public class FlickrResponseModel {
    private FlickrPhotosModel photos;
    private String stat;

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public FlickrPhotosModel getPhotos() {
        return photos;
    }

    public void setPhotos(FlickrPhotosModel photos) {
        this.photos = photos;
    }
}
