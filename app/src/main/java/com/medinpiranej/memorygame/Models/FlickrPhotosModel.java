package com.medinpiranej.memorygame.Models;

/**
 * Created by Medin Piranej.
 */

public class FlickrPhotosModel {
    private int page;
    private int pages;
    private int perpage;
    private String total;
    private FlickrPhotoModel [] photo;

    public FlickrPhotoModel[] getPhoto() {
        return photo;
    }

    public void setPhoto(FlickrPhotoModel[] photo) {
        this.photo = photo;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public int getPerpage() {
        return perpage;
    }

    public void setPerpage(int perpage) {
        this.perpage = perpage;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
