package com.medinpiranej.memorygame;

import android.support.annotation.NonNull;
import android.util.Log;

import com.medinpiranej.memorygame.Models.FlickrPhotoModel;

import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;

/**
 * Created by Medin Piranej
 */

public class FlickrApi {

    private static final String API_KEY = "dd4f30bf4877bdd9798aea3f7a9ec984";

    public static final String TAG = "FlickrApi.class";

    public static void getFlickrPhotos(String tag, int photosPerPage, Callback callback){
        String url = "https://api.flickr.com/services/rest/?method=flickr.photos.search"
                + "&api_key=" + API_KEY
                + "&text=" + tag
                + "&per_page=" + photosPerPage
                + "&format=json&media=photos&nojsoncallback=1";
        Log.d(TAG,"Sending FlickrApi Request with url = " + url);
        new OkHttpClient().newCall(new Request.Builder().url(url).build()).enqueue(callback);
    }

    public static String buildUrl(@NonNull FlickrPhotoModel model, boolean onlyThumb){
        String url = "http://farm"
                + model.getFarm()
                + ".static.flickr.com/"
                + model.getServer()
                + "/"
                + model.getId()
                + "_"
                + model.getSecret()
                + (onlyThumb? "_s" : "")
                + ".jpg";
        Log.d(TAG,"Building photo url : " + url);
        return url;
    }
}
