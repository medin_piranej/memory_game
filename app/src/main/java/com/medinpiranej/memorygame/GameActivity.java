package com.medinpiranej.memorygame;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.medinpiranej.memorygame.Models.FlickrPhotoModel;
import com.medinpiranej.memorygame.Models.FlickrResponseModel;
import com.medinpiranej.memorygame.Models.PlayerModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Medin Piranej
 */

public class GameActivity extends AppCompatActivity {

    public final String TAG = "GameActivity.class";

    List<RelativeLayout> openCards = new ArrayList<>();

    List<RelativeLayout> cards = new ArrayList<>();

    TableLayout tableLayout;

    List<FlickrPhotoModel> flickrPhotos = new ArrayList<>();

    TextView timeElapsed;
    TextView moves;
    TextView remainingPairs;

    int movesCount = 0;
    int remainingPairsCount = 10;
    long timeStartedMillis;

    boolean shouldCountTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        timeElapsed = (TextView) findViewById(R.id.timeElapsed);
        moves = (TextView) findViewById(R.id.moves);
        remainingPairs = (TextView) findViewById(R.id.remainingPairs);

        timeElapsed.setText("00 : 00");
        moves.setText("0");
        remainingPairs.setText("10");

        tableLayout = (TableLayout) findViewById(R.id.tableLayout);

        // getting cards from table layout
        for(int i = 0; i < tableLayout.getChildCount(); i++){
            if(tableLayout.getChildAt(i) instanceof TableRow){
                TableRow tableRow = (TableRow) tableLayout.getChildAt(i);
                for(int j = 0 ; j < tableRow.getChildCount(); j++){
                    if(tableRow.getChildAt(j) instanceof RelativeLayout){
                        cards.add((RelativeLayout) tableRow.getChildAt(j));
                    }
                }
            }
        }

        try {
            FlickrPhotoModel[] flickrPhotoModels = new Gson()
                    .fromJson(getIntent().getExtras().getString("photos"), FlickrResponseModel.class)
                    .getPhotos().getPhoto();
            // Starting to mix photos
            Random random = new Random();
            int nextInt;
            for(int i = 0; i < flickrPhotoModels.length; i++) {
                // first card
                nextInt = random.nextInt(cards.size());
                cards.get(nextInt).setTag(flickrPhotoModels[i]);
                Picasso.with(this)
                        .load(FlickrApi.buildUrl(flickrPhotoModels[i],true))
                        .into(((ImageView) cards.get(nextInt).getChildAt(1)));
                cards.remove(cards.get(nextInt));
                // second card
                nextInt = random.nextInt(cards.size());
                cards.get(nextInt).setTag(flickrPhotoModels[i]);
                Picasso.with(this)
                        .load(FlickrApi.buildUrl(flickrPhotoModels[i],true))
                        .into(((ImageView) cards.get(nextInt).getChildAt(1)));
                cards.remove(cards.get(nextInt));
            }
        }
        catch (Exception e){
            e.printStackTrace();
            Toast.makeText(getApplicationContext(),"Could not load photos !, Please try again !",Toast.LENGTH_LONG).show();
            finish();
        }
    }

    void countAndUpdateTime(){
        long diff = System.currentTimeMillis() - timeStartedMillis;
        int minutes = (int) diff / 60000;
        int seconds = (int) (diff % 60000) / 1000;
        timeElapsed.setText("" + minutes + " : " + seconds);
        if(shouldCountTime){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    countAndUpdateTime();
                }
            },1000);
        }
     }

    public void onCardClick(View v){
        final RelativeLayout currentCard = (RelativeLayout) v;
        final ImageView imageView = (ImageView) currentCard.getChildAt(1);
        if(imageView.getAlpha() != 1){ // card is closed
            if(!shouldCountTime){ // check if this is the first move in order to start counting
                shouldCountTime = true;
                timeStartedMillis = System.currentTimeMillis();
                countAndUpdateTime();
                Log.d(TAG,"Started counting time");
            }
            movesCount++;
            moves.setText(String.valueOf(movesCount));
            if(openCards.size() == 1){
                openCards.add(currentCard);
                if(openCards.get(0).getTag() == currentCard.getTag()){
                    disableOpenCards();
                    remainingPairsCount--;
                    remainingPairs.setText(String.valueOf(remainingPairsCount));
                    if(remainingPairsCount == 0){
                        shouldCountTime = false;
                        PlayerModel playerModel = new PlayerModel();
                        playerModel.setName(getIntent().getExtras().getString("username"));
                        playerModel.setBestScore(((System.currentTimeMillis() - timeStartedMillis)/1000) + movesCount);
                        GenericUtils.showInfoPopup(GameActivity.this
                                ,"Congratulations " + playerModel.getName()
                                ,"You finished the game with " + playerModel.getBestScore()
                                    + "\n(Score is calculated seconds + moves)");
                        LocalDatabaseManager.setLastPlayer(GameActivity.this,playerModel);
                    }
                }
                else{
                    closeOpenCards();
                }
            }
            else{
                openCards.add(currentCard);
            }
            openCard(currentCard);
        }
        else{
            closeCard(currentCard);
            openCards.remove(currentCard);
        }
    }

    void disableOpenCards(){
        for(RelativeLayout currentCard : openCards){
            disableCard(currentCard);
        }
        openCards.clear();
    }

    void closeOpenCards(){
        for(final RelativeLayout currentCard : openCards){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    closeCard(currentCard);
                }
            },1500);
        }
        openCards.clear();
    }

    void openCard(final RelativeLayout card){
        final ImageView imageView = (ImageView) card.getChildAt(1);
        card.animate().rotationY(90).withEndAction(new Runnable() {
            @Override
            public void run() {
                card.animate().rotationY(0);
                imageView.setAlpha(1f);
            }
        });
    }

    void closeCard(final RelativeLayout card){
        final ImageView imageView = (ImageView) card.getChildAt(1);
        card.animate().rotationY(90).withEndAction(new Runnable() {
            @Override
            public void run() {
                card.animate().rotationY(180);
                imageView.setAlpha(0f);
            }
        });
    }

    void disableCard(final RelativeLayout card){
        card.animate().alpha(0.6f);
        card.setOnClickListener(null);
    }
}
